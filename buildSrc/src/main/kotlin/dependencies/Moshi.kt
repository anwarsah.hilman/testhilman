package dependencies


import org.gradle.api.artifacts.dsl.DependencyHandler
import kaptAndroidTest
import implementation
import kapt
import kaptTest
import testImplementation
import androidTestImplementation

fun DependencyHandler.moshi(){

    val moshi = "1.14.0"
    implementation("com.squareup.moshi:moshi:$moshi")
    kapt("com.squareup.moshi:moshi-kotlin-codegen:$moshi")
    implementation("com.squareup.moshi:moshi-kotlin:$moshi")
}
