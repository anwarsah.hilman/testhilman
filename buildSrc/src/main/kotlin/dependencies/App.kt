package dependencies

import org.gradle.api.artifacts.dsl.DependencyHandler

fun DependencyHandler.appDeps() {
    androidCore()
    daggerHilt()
    mapStruct()
    moshi()
    okHttp()
    paging()
    retrofit()
    compose()
    workManager()
}