package dependencies

import org.gradle.api.artifacts.dsl.DependencyHandler

import kaptAndroidTest
import implementation
import kapt
import kaptTest
import testImplementation
import androidTestImplementation

fun DependencyHandler.androidCore(){

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.10.0")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}