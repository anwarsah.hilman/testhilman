package dependencies


import org.gradle.api.artifacts.dsl.DependencyHandler
import kaptAndroidTest
import implementation
import kapt
import kaptTest
import testImplementation
import androidTestImplementation

fun DependencyHandler.okHttp(){
    val okHttpVersion = "4.10.0"
    implementation("com.squareup.okhttp3:okhttp:$okHttpVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:$okHttpVersion")
    testImplementation("com.squareup.okhttp3:mockwebserver:$okHttpVersion")
}