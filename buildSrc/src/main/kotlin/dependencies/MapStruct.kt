package dependencies


import org.gradle.api.artifacts.dsl.DependencyHandler
import kaptAndroidTest
import implementation
import kapt
import kaptTest
import testImplementation
import androidTestImplementation

fun DependencyHandler.mapStruct(){

    val mapstructVersion = "1.5.5.Final"
    implementation("org.mapstruct:mapstruct:${mapstructVersion}")
    kapt("org.mapstruct:mapstruct-processor:${mapstructVersion}")
}