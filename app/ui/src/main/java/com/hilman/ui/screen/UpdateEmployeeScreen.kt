package com.hilman.ui.screen

import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.hilman.domain.usecase.employee.model.EmployeeModel
import com.hilman.ui.theme.LocalColorScheme

@ExperimentalMaterial3Api
@Composable
fun UpdateEmployeeScreen_Screen(
    isLoading: Boolean,
    error: String? = null, getAge: String = "",
    getName: String = "",
    getSalary: String = "",
    setAge: (String) -> Unit = {},
    setName: (String) -> Unit = {},
    onBackClick: () -> Unit = {},
    setSalary: (String) -> Unit = {}, onSubmit: () -> Unit = {}
) {

    BoxWithConstraints {
        EmployeeForm_Screen(
            title = "Update Employee",
            isLoading = isLoading,
            onSubmit = {
                onSubmit()
            },
            onBackClick = onBackClick,
            error = error,
            getAge = getAge,
            getName = getName,
            getSalary = getSalary,
            setSalary = setSalary,
            setName = setName,
            setAge = setAge
        )

        if (isLoading) {
            CircularProgressIndicator(
                color = LocalColorScheme.current.primary,
                modifier = Modifier
                    .align(Alignment.Center)
                    .size(48.dp)
            )
        }
    }
}


@ExperimentalMaterial3Api
@Composable
fun UpdateEmployeeScreen(
    navController: NavController,
    vm: UpdateEmployeeViewModel,
    listVm: ListEmployeeViewModel,
    args:EmployeeModel?
) {
    val state by vm.state

    LaunchedEffect(key1 = state, block = {
        if (state.isLoaded()) {
            listVm.update(state.data)
            navController.popBackStack()
        }
    })

    UpdateEmployeeScreen_Screen(
        isLoading = state.isLoading(),
        error = if (state.isError()) state.getError()?.message.orEmpty() else null,
        getSalary = vm.salary.value,
        setSalary = { vm.salary.value = it },
        getName = vm.name.value,
        setName = { vm.name.value = it },
        getAge = vm.age.value,
        onBackClick = {
            navController.popBackStack()
        },
        setAge = {
            vm.age.value = it
        }
    ) {
        vm.submit(args)
    }


}


@ExperimentalMaterial3Api
@Composable
@Preview
fun UpdateEmployeeScreen_Preview() {
    UpdateEmployeeScreen_Screen(
        isLoading = true
    )
}

