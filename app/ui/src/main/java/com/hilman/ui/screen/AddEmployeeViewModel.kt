package com.hilman.ui.screen

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hilman.domain.usecase.employee.CreateEmployeeUseCase
import com.hilman.domain.usecase.employee.model.EmployeeModel
import com.hilman.domain.usecase.employee.model.LoadState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddEmployeeViewModel @Inject constructor(val addEmployeeUseCase: CreateEmployeeUseCase) :
    ViewModel() {
    val state: MutableState<LoadState<EmployeeModel>> = mutableStateOf(LoadState.None())
    val name = mutableStateOf("")
    val age = mutableStateOf("")
    val salary = mutableStateOf("")

    fun submit() {
        viewModelScope.launch {
            addEmployeeUseCase(
                age.value,
                name.value,
                salary.value
            ).collect {
                state.value = it
            }
        }
    }
}