package com.hilman.ui.theme

import androidx.compose.material3.ColorScheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

object AppColor {
    val colorSecondary: Color = Color(0xffF2C94C)
    val colorPrimary: Color = Color(0xffF2C94C)
    val surface: Color = Color(0xFFFBFBFB)
    val errorColor: Color = Color.Red
    val textColor: Color = Color.Black
    val Gray1 = Color(0xFF828282)
    val NeutralBlack = Color(0xFF333333)
    val Red = Color(0xFFF44336)
    val Gray2 = Color(0xFFBDBDBD)
    val Blue1 = Color(0xFF2F80ED)
    val Yellow = Color(0xFFF2C94C)
}


val AppLightColorScheme: ColorScheme
    get() = lightColorScheme(
        primary = AppColor.colorPrimary,
        secondary = AppColor.colorSecondary,
        surface = AppColor.surface,
        onSurface = AppColor.textColor
    )


val LocalColorScheme = staticCompositionLocalOf { lightColorScheme() }
val LocalAppColor = compositionLocalOf { AppColor }
val LocalTypography = compositionLocalOf { Typography }
