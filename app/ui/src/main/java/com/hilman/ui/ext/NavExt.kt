package com.hilman.ui.ext

import android.os.Parcelable
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController

fun NavController.navigateWithPayload(route: String, vararg args: Pair<String, Parcelable>) {
    navigate(route)

    requireNotNull(currentBackStackEntry?.arguments).apply {
        args.forEach { (key: String, arg: Parcelable) ->
            putParcelable(key, arg)
        }
    }
}

