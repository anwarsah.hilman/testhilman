package com.hilman.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.rounded.KeyboardBackspace
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hilman.ui.theme.LocalAppColor
import com.hilman.ui.theme.Typography

@Composable
@Preview
fun EmployeeAppBar(title: String = "",  onBackClick: () -> Unit = {},onAddCLick: () -> Unit={}) {
    Column {
        TopAppBar(title = title, onBackClick = onBackClick, menuItem = {
            Row(
                modifier = Modifier.align(Alignment.CenterEnd)
            ) {
                IconButton(
                    onClick = onAddCLick
                ) {
                    androidx.compose.material.Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = "Download",
                    )
                }
            }
        })
    }
}



@Composable
@Preview
fun AddEmployeeAppBar(title: String = "", onBackClick: () -> Unit = {}) {
    Column {
        TopAppBar(title = title, onBackClick = onBackClick)
    }
}

@Composable
fun TopAppBar(
    onBackClick: () -> Unit = {},
    title: String,
    navIcon: ImageVector = Icons.Rounded.KeyboardBackspace,
    menuItem: @Composable BoxScope.() -> Unit = {}
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.background(LocalAppColor.current.Yellow)
    ) {
        Text(
            text = title,
            textAlign = TextAlign.Center,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = Typography.titleMedium,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 44.dp)
        )
        IconButton(
            onClick = onBackClick,
            colors = IconButtonDefaults.iconButtonColors(
                contentColor = LocalAppColor.current.Blue1
            ),
            modifier = Modifier
                .padding(start = 6.dp)
                .size(44.dp)
                .align(Alignment.CenterStart)
        ) {
            Icon(
                imageVector = navIcon,
                contentDescription = null
            )
        }
        menuItem()
    }
}