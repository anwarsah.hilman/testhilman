package com.hilman.ui.components


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Error
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.hilman.ui.theme.LocalAppColor
import com.hilman.ui.theme.LocalColorScheme
import com.hilman.ui.theme.LocalTypography


@Composable
fun LoaderLayout(
    modifier: Modifier = Modifier,
    isLoading: Boolean = true,
    isError: Boolean = false,
    errorTitle: String = "Error",
    errorMessage: String = "Something wrong!",
    onRetryClick: () -> Unit = {},
    content: @Composable () -> Unit = {}
) {
    BoxWithConstraints(modifier) {
        if (isLoading) {
            CircularProgressIndicator(
                color = LocalColorScheme.current.primary,
                modifier = Modifier
                    .size(48.dp)

                    .align(Alignment.Center)
            )
        }
        if (isError) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(20.dp)
                    .align(Alignment.Center)
            ) {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .size(50.dp)
                        .background(color = LocalAppColor.current.Red, shape = CircleShape)
                        .clip(CircleShape)
                ) {
                    Icon(
                        imageVector = Icons.Rounded.Error,
                        contentDescription = null,
                        tint = Color.White,
                        modifier = Modifier.fillMaxSize()
                    )
                }

                if (errorTitle.isNotBlank()) {
                    Text(
                        text = errorTitle,
                        style = LocalTypography.current.titleSmall,
                        color = LocalAppColor.current.NeutralBlack,
                        modifier = Modifier.padding(top = 25.dp)
                    )
                }

                if (errorMessage.isNotBlank()) {
                    Text(
                        text = errorMessage,
                        style = LocalTypography.current.labelSmall.copy(fontSize = 12.sp),
                        color = LocalAppColor.current.Gray2,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .padding(top = 10.dp)
                            .fillMaxWidth()
                    )
                }
                Button(
                    onClick = onRetryClick,
                    modifier = Modifier.padding(top = 10.dp)
                ) {
                    Text(
                        text = "Retry",
                        style = LocalTypography.current.labelSmall
                    )
                }
            }
        }
        if (isLoading.not() && isError.not()) {
            content()
        }
    }
}
