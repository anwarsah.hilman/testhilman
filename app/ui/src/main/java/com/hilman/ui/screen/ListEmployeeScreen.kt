@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package com.hilman.ui.screen

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardColors
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavController
import com.hilman.domain.usecase.employee.model.EmployeeModel
import com.hilman.ui.components.EmployeeAppBar
import com.hilman.ui.components.LoaderLayout
import com.hilman.ui.ext.navigateWithPayload
import com.hilman.ui.theme.LocalAppColor
import com.hilman.ui.theme.LocalColorScheme


@Composable
fun ListEmployeeScreen(navController: NavController, vm: ListEmployeeViewModel) {
    val data by vm.state
    val deleting by vm.deletingModel
    val deleteState by vm.deleteState
    ListEmployeeScreen_Screen(
        isLoading = data.isLoading(),
        isError = data.isError(),

        errorMessage = data.getError()?.message.orEmpty(),
        onAddCLick = {
            navController.navigate("add")
        },
        onEditClick = {
            navController.navigateWithPayload("edit", "data" to it)
        },
        onDeleteClick = {
            vm.deletingModel.value = it
        },
        data = data.getLoadedData().orEmpty(),
        isDeleting = deleteState.isLoading()
    ) {
        vm.loadEmployee()
    }
    LaunchedEffect(key1 = deleteState, block = {
        if(deleteState.isLoaded()){

        }
    })
    DeleteDialog(
        deleting = deleting,
        isDeletingError = deleteState.isError(),
        errorMessage = deleteState.getError()?.message.orEmpty(),
        onDeleteClick = {
            vm.delete()
        },
        onDismiss = {
            vm.deletingModel.value = null
        }
    )


}

@Composable
fun DeleteDialog(
    deleting: EmployeeModel?,
    isDeletingError: Boolean,
    errorMessage: String?,
    onDeleteClick: () -> Unit,
    onDismiss: () -> Unit = {}
) {
    deleting?.let {
        Dialog(onDismissRequest = {
            onDismiss()
        },properties = DialogProperties(dismissOnClickOutside=false)) {
            Card(colors = CardDefaults.cardColors(containerColor = LocalAppColor.current.surface)) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp)
                ) {
                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = "Are you sure you want to delete this item?",
                        textAlign = TextAlign.Center
                    )
                    if (isDeletingError) {
                        Text(
                            modifier = Modifier.fillMaxWidth(),
                            text = errorMessage.orEmpty(),
                            textAlign = TextAlign.Center,
                            color = LocalAppColor.current.errorColor
                        )
                    }


                }

                Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                    Button(onClick = {
                        onDeleteClick()
                    }, modifier = Modifier.padding(5.dp)) {
                        Text(text = "Yes")
                    }
                    Button(onClick = {
                        onDismiss()
                    }, modifier = Modifier.padding(5.dp)) {
                        Text(text = "Cancel")
                    }
                }
            }

        }
    }
}

@ExperimentalMaterial3Api
@Composable
fun ListEmployeeScreen_Screen(
    isLoading: Boolean,
    isError: Boolean,
    errorMessage: String = "",
    data: List<EmployeeModel>,
    onAddCLick: () -> Unit = {},
    onDeleteClick: (EmployeeModel) -> Unit = {},
    onEditClick: (EmployeeModel) -> Unit = {},
    isDeleting: Boolean,
    onRetry: () -> Unit,
) {
    Log.e("REcompose", "${data}")
    val state = rememberLazyListState(5, 5)
    val scrollState = rememberScrollState()
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        Scaffold(topBar = {
            EmployeeAppBar(title = "List Employee", onAddCLick = onAddCLick)
        }, modifier = Modifier.fillMaxSize()) {
//        BoxWithConstraints {
            LoaderLayout(
                modifier = Modifier
                    .padding(it)
                    .fillMaxSize(),
                isLoading = isLoading,
                errorMessage = errorMessage,
                isError = isError,
                onRetryClick = onRetry
            ) {
//                Column(modifier = Modifier.verticalScroll(scrollState)) {
//                    repeat(data.size) {index->
//                        val item = data[index]
//                        EmployeeCard(
//                            id = item.id.toString(),
//                            name = item.employeeName.toString(),
//                            salary = item.employeeSalary.toString(),
//                            age = item.employeeAge.toString(),
//                            onEditClick = {
//                                onEditClick(item)
//                            },
//                            onDeleteClick = {
//                                onDeleteClick(item)
//                            }
//                        )
//                    }
//                }

                LazyColumn() {
                    items(
                        count = data.size,
                        key = {
                            data[it].hashCode()
                        },
                        itemContent = { index ->
                            val item = data[index]
                            EmployeeCard(
                                id = item.id.toString(),
                                name = item.employeeName.toString(),
                                salary = item.employeeSalary.toString(),
                                age = item.employeeAge.toString(),
                                onEditClick = {
                                    onEditClick(item)
                                },
                                onDeleteClick = {
                                    onDeleteClick(item)
                                }
                            )

                        }
                    )
                }


//                LazyColumn(
//
//                    state = state
////                    content = {
////                        lazy { }
////                        items(
////                            key = {
////                                data[it].id ?: 0
////                            },
////                            count = data.size,
////                            itemContent = {
////
////
////                            }
////                        )
////                    }
//                ) {
//                    items(data, key = {
//                        Log.e("PRINTID","${it.id ?: 0}")
//                        it.id ?: 0
//                    }, itemContent = {
//
//                    })
//                }
            }

        }
        if (isDeleting) {
            CircularProgressIndicator(
                color = LocalColorScheme.current.primary,
                modifier = Modifier
                    .size(48.dp)
                    .align(Alignment.Center)
            )
        }
    }


}

@Composable
@Preview
fun EmployeeCard(
    id: String = "", name: String = "", salary: String = "", age: String = "",
    onDeleteClick: () -> Unit = {},
    onEditClick: () -> Unit = {}
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp),
        colors = CardDefaults.cardColors(containerColor = LocalAppColor.current.Gray2)
    ) {
        Column(modifier = Modifier.padding(10.dp)) {
            Column {

                Text(text = "ID: ${id}", modifier = Modifier.padding(vertical = 5.dp))
                Text(
                    text = "Name: ${name}",
                    modifier = Modifier.padding(vertical = 5.dp)
                )
                Text(
                    text = "Salary: ${salary}",
                    modifier = Modifier.padding(vertical = 5.dp)
                )
                Text(
                    text = "Age: ${age}",
                    modifier = Modifier.padding(vertical = 5.dp)
                )
            }
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                Button(onClick = {
                    onEditClick()
                }, modifier = Modifier.padding(5.dp)) {
                    Text(text = "Edit")
                }
                Button(onClick = {
                    onDeleteClick()
                }, modifier = Modifier.padding(5.dp)) {
                    Text(text = "Delete")
                }
            }
        }
    }
}


@ExperimentalMaterial3Api
@Composable
@Preview
fun ListEmployeeScreen_Preview() {
    ListEmployeeScreen_Screen(
        true, false,
        errorMessage = "",
        listOf(EmployeeModel("1", "1", "1", 1, "1")),
        isDeleting = true
    ) {}
}

