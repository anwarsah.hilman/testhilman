package com.hilman.ui.screen

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hilman.domain.usecase.employee.DeleteEmployeeUseCase
import com.hilman.domain.usecase.employee.GetEmployeeUseCase
import com.hilman.domain.usecase.employee.model.EmployeeModel
import com.hilman.domain.usecase.employee.model.LoadState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListEmployeeViewModel @Inject constructor(
    val getEmployeeUseCase: GetEmployeeUseCase,
    val deleteEmployeeUseCase: DeleteEmployeeUseCase
) :
    ViewModel() {


    val state: MutableState<LoadState<List<EmployeeModel>>> =
        mutableStateOf(LoadState.Loading())
    val deletingModel = mutableStateOf<EmployeeModel?>(null)
    val deleteState = mutableStateOf<LoadState<Any>>(LoadState.None())

    init {
        loadEmployee()
    }

    fun loadEmployee() {
        viewModelScope.launch {
            getEmployeeUseCase().collect {
                state.value = it
            }
        }
    }

    fun delete() {
        deletingModel.value?.let { delete ->
            viewModelScope.launch {
                deleteEmployeeUseCase(delete.id.toString()).collect {
                    deleteState.value = it

                    if (it.isLoaded()) {
                        state.value.apply {
                            this.data.orEmpty().toMutableList().apply {
                                this.remove(delete)
                                state.value = LoadState.Loaded(this)
                            }
                        }
                        deletingModel.value = null
                    }
                }
            }
        }

    }

    fun update(data: EmployeeModel?) {
        data?.let {
            state.value.data.orEmpty().orEmpty().toMutableList().apply {
                this.indexOfFirst { it.id == data.id }?.let {
                    if (it > 0) {
                        this[it] = data
                        state.value = LoadState.Loaded(this)
                    }
                }
            }
        }

    }
}