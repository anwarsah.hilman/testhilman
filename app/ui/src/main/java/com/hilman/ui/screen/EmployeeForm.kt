package com.hilman.ui.screen


import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hilman.domain.usecase.employee.model.EmployeeModel
import com.hilman.ui.components.AddEmployeeAppBar
import com.hilman.ui.theme.LocalAppColor

@ExperimentalMaterial3Api
@Composable
fun EmployeeForm_Screen(
    title: String = "",
    onBackClick: () -> Unit = {},
    isLoading: Boolean = false,
    getAge: String = "",
    getName: String = "",
    getSalary: String = "",
    setAge: (String) -> Unit = {},
    setName: (String) -> Unit = {},
    setSalary: (String) -> Unit = {},
    error: String? = null,
    onSubmit: () -> Unit = {}
) {
    Scaffold(topBar = {
        AddEmployeeAppBar(title = title,onBackClick=onBackClick)
    }) {
        Surface(color = LocalAppColor.current.surface, modifier = Modifier.padding(it)) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 30.dp)
                ) {
                    TextField(value = getName, placeholder = {
                        Text(text = "Name")
                    }, onValueChange = {
                        setName(it)
                    },
                        enabled = !isLoading, modifier = Modifier
                            .padding(5.dp)
                            .fillMaxWidth()
                    )
                    TextField(
                        enabled = !isLoading,
                        value = getSalary,
                        placeholder = {
                            Text(text = "Salary")
                        },
                        onValueChange = {
                            setSalary(it)
                        },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier
                            .padding(5.dp)
                            .fillMaxWidth()
                    )
                    TextField(
                        enabled = !isLoading,
                        value = getAge,
                        placeholder = {
                            Text(text = "Age")
                        },
                        onValueChange = {
                            kotlin.runCatching {
                                it.toInt()
                            }.onSuccess {
                                if (it < 100) {
                                    setAge(it.toString())
                                }
                            }

                        },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier
                            .padding(5.dp)
                            .fillMaxWidth()
                    )
                    error?.let {
                        Text(
                            text = it,
                            color = LocalAppColor.current.errorColor,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(10.dp),
                            textAlign = TextAlign.Center
                        )
                    }

                    Button(onClick = {
                        onSubmit()
                    }, modifier = Modifier.fillMaxWidth()) {
                        Text(text = "Submit")
                    }
                }
            }
        }
    }


}


@ExperimentalMaterial3Api
@Composable
@Preview
fun EmployeeForm_Preview() {
    EmployeeForm_Screen(
    )
}

