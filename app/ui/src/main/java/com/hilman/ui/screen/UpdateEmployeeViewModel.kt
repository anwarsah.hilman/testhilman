package com.hilman.ui.screen

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hilman.domain.usecase.employee.UpdateEmployeeUseCase
import com.hilman.domain.usecase.employee.model.EmployeeModel
import com.hilman.domain.usecase.employee.model.LoadState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UpdateEmployeeViewModel @Inject constructor(val updateEmployeeUseCase: UpdateEmployeeUseCase) :
    ViewModel() {
    val state: MutableState<LoadState<EmployeeModel>> = mutableStateOf(LoadState.None())
    val name = mutableStateOf("")
    val age = mutableStateOf("")
    val salary = mutableStateOf("")
    val id = mutableStateOf("")

    fun submit(args: EmployeeModel?) {
        viewModelScope.launch {
            updateEmployeeUseCase(
                id.value.orEmpty(),
                age.value,
                name.value,
                salary.value
            ).collect {
                args?.employeeAge = age.value
                args?.employeeName = name.value
                args?.employeeSalary = salary.value
                it.data = args
                state.value = it
            }
        }
    }
}