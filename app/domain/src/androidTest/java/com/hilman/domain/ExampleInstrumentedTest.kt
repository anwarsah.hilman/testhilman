package com.hilman.domain

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.hilman.domain.usecase.employee.CreateEmployeeUseCase
import com.hilman.domain.usecase.employee.DeleteEmployeeUseCase
import com.hilman.domain.usecase.employee.GetEmployeeByIdUseCase
import com.hilman.domain.usecase.employee.GetEmployeeUseCase
import com.hilman.domain.usecase.employee.UpdateEmployeeUseCase
import com.hilman.domain.usecase.employee.model.LoadState
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class ExampleInstrumentedTest {

    @Inject
    lateinit var createEmployeeUseCase: CreateEmployeeUseCase


    @Inject
    lateinit var deleteEmployeeUseCase: DeleteEmployeeUseCase


    @Inject
    lateinit var getEmployeeByIdUseCase: GetEmployeeByIdUseCase


    @Inject
    lateinit var getEmployeeUseCase: GetEmployeeUseCase


    @Inject
    lateinit var updateEmployeeUseCase: UpdateEmployeeUseCase

    private val delay: Long = 60

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun start() {
        hiltRule.inject()
    }

    private fun <T> assertLoadState(state: LoadState<T>) {
        if (state.isLoaded() || state.isError()) {
            assert(state.isLoaded()) {
                (state as LoadState.Error).exception.run {
                    this.printStackTrace()
                    this.message.orEmpty()
                }
            }
        }
    }


    @Test
    fun testGetEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val z = getEmployeeUseCase()
            z.collect {
                assertLoadState(it)
            }
        }
    }

    @Test
    fun testGetEmployeeByUId() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val z = getEmployeeByIdUseCase("1")
            z.collect {
                assertLoadState(it)
            }
        }
    }

    @Test
    fun testCreateEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val z = createEmployeeUseCase("1", "Tezzz", "20000")
            z.collect {
                assertLoadState(it)
            }
        }
    }

    @Test
    fun testEditEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val z = updateEmployeeUseCase("1", "3", "Tezzz", "20000")
            z.collect {
                assertLoadState(it)
            }
        }
    }

    @Test
    fun testCreateAndDeleteEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val z = createEmployeeUseCase("1", "Tezzz", "20000")
            z.collect {
                if (it.isLoaded()) {
                    delay(TimeUnit.SECONDS.toMillis(delay))
                    val deleteResult =
                        deleteEmployeeUseCase(it.data?.id?.toString().orEmpty())
                    deleteResult.collect {
                        assertLoadState(it)
                    }
                } else if (it.isLoading()) {
                    println("loading")
                } else {
                    assert(false)
                }
            }
        }
    }
}