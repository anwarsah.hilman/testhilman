package com.hilman.domain.usecase.employee.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class EmployeeModel(
    var employeeAge: String?,
    var employeeName: String?,
    var employeeSalary: String?,
    var id: Int?,
    var profileImage: String?
):Parcelable{


    override fun hashCode(): Int {
        var result = employeeAge?.hashCode() ?: 0
        result = 31 * result + (employeeName?.hashCode() ?: 0)
        result = 31 * result + (employeeSalary?.hashCode() ?: 0)
        result = 31 * result + (id ?: 0)
        result = 31 * result + (profileImage?.hashCode() ?: 0)
        return result
    }
}