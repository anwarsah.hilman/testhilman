package com.hilman.domain.usecase.employee.model


sealed class LoadState<T>(var data: T? = null) {
    class Loading<T> : LoadState<T>()
    class Loaded<T>(private val appData: T) : LoadState<T>(appData)
    class Error<T>(val exception: Throwable) : LoadState<T>() {
        init {
            exception.printStackTrace()
        }
    }

    class None<T> : LoadState<T>()

    fun isLoading() = this is Loading

    fun isLoaded() = this is Loaded


    fun isError() = this is Error

    fun getLoadedData() =
        (this as? Loaded<T>)?.data

    fun getError() =
        (this as? Error<T>)?.exception

}
