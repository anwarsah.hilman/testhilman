package com.hilman.domain.usecase.employee

import com.hilman.data.remote.EmployeeDataSource
import com.hilman.domain.usecase.employee.model.EmployeeMapper
import com.hilman.ext.asModel
import com.hilman.ext.useCaseFlow
import kotlinx.coroutines.flow.flow

class GetEmployeeUseCase(
    private val employeeDataSource: EmployeeDataSource,
    private val employeeMapper: EmployeeMapper
) {

    suspend operator fun invoke() = useCaseFlow(mapper = {
        employeeMapper.mapEmployees(it)
    }) {
        employeeDataSource.getEmployee()
    }

}