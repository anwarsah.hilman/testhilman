package com.hilman.domain.usecase.employee

import com.hilman.data.remote.EmployeeDataSource
import com.hilman.domain.usecase.employee.model.EmployeeMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class _Module {

    @Provides
    fun provideCreateEmployeeUseCase(
        employeeMapper: EmployeeMapper,
        employeeDataSource: EmployeeDataSource
    ) = CreateEmployeeUseCase(employeeDataSource, employeeMapper)


    @Provides
    fun provideDeleteEmployeeUseCase(
        employeeDataSource: EmployeeDataSource
    ) = DeleteEmployeeUseCase(employeeDataSource)


    @Provides
    fun provideGetEmployeeByIdUseCase(
        employeeMapper: EmployeeMapper,
        employeeDataSource: EmployeeDataSource
    ) = GetEmployeeByIdUseCase(employeeDataSource, employeeMapper)


    @Provides
    fun provideGetEmployeeUseCase(
        employeeMapper: EmployeeMapper,
        employeeDataSource: EmployeeDataSource
    ) = GetEmployeeUseCase(employeeDataSource, employeeMapper)


    @Provides
    fun provideUpdateEmployeeUseCase(
        employeeMapper: EmployeeMapper,
        employeeDataSource: EmployeeDataSource
    ) = UpdateEmployeeUseCase(employeeDataSource,employeeMapper)

}