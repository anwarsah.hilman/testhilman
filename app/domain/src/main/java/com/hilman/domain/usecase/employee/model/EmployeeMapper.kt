package com.hilman.domain.usecase.employee.model

import com.hilman.data.remote.response.employee.Employee
import org.mapstruct.Mapper
import org.mapstruct.control.DeepClone


@Mapper(mappingControl = DeepClone::class)
interface EmployeeMapper {

    fun mapEmployee(employee: Employee): EmployeeModel
    fun mapEmployees(employee: List<Employee>): List<EmployeeModel>
}