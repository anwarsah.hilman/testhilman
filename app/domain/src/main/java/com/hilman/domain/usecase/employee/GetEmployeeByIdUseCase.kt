package com.hilman.domain.usecase.employee

import com.hilman.data.remote.EmployeeDataSource
import com.hilman.domain.usecase.employee.model.EmployeeMapper
import com.hilman.ext.asModel
import com.hilman.ext.useCaseFlow
import kotlinx.coroutines.flow.flow

class GetEmployeeByIdUseCase(
    private val employeeDataSource: EmployeeDataSource,
    private val employeeMapper: EmployeeMapper
) {
    suspend operator fun invoke(id: String) = useCaseFlow(mapper = {
        employeeMapper.mapEmployee(it)
    }) {
        employeeDataSource.getEmployee(id)
    }
}