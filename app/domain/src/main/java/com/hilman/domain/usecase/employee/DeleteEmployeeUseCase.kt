package com.hilman.domain.usecase.employee

import com.hilman.data.remote.EmployeeDataSource
import com.hilman.ext.useCaseFlow
import kotlinx.coroutines.flow.flow

class DeleteEmployeeUseCase(private val employeeDataSource: EmployeeDataSource) {
    suspend operator fun invoke(id: String) = useCaseFlow<Any, Any> {
        employeeDataSource.deleteEmployee(id)
    }
}