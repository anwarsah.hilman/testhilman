package com.hilman.domain.usecase.employee.model

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import org.mapstruct.factory.Mappers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class _Module {

    @Provides
    @Singleton
    fun provideEmployeeMapper(): EmployeeMapper {
        return Mappers.getMapper(EmployeeMapper::class.java)
    }


}