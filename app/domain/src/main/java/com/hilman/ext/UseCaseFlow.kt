package com.hilman.ext

import com.hilman.domain.usecase.employee.model.LoadState
import kotlinx.coroutines.flow.flow

fun <T, R> useCaseFlow(mapper: (T) -> R = { it as R }, useCase: suspend () -> Result<T>) = flow {
    emit(LoadState.Loading())
    emit(
        useCase().asModel {
            mapper(it)
        }.run {
            if (isSuccess) {
                LoadState.Loaded(this.getOrThrow())
            } else {
                this.exceptionOrNull()?.let {
                    LoadState.Error<R>(it)
                } ?: LoadState.Error(Exception("Empty Response"))
            }
        }
    )
}
