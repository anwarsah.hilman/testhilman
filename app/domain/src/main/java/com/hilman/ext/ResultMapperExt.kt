package com.hilman.ext

fun <T, E> Result<T>.asModel(mapper: (T) -> E): Result<E> {
    if (isSuccess) {
        return Result.success(mapper(getOrThrow()))
    } else {
        return exceptionOrNull()?.let {
            Result.failure(it)
        } ?: Result.failure(Exception("Empty Result"))
    }
}