@file:OptIn(ExperimentalLayoutApi::class)

package com.hilman.devtest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.hilman.devtest.ext.rememberViewModelStoreOwner
import com.hilman.devtest.graph.appGraph
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
@ExperimentalMaterial3Api
class MainActivity : ComponentActivity() {


    private lateinit var controller: NavHostController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
//            HilmanTestTheme {
            AppNavHost()
//            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterialApi::class)
@ExperimentalMaterial3Api
@Composable
fun AppNavHost(

) {
    val stateOwner = rememberViewModelStoreOwner()
    val startDestination = "list"

    val controller = rememberNavController()
    NavHost(navController = controller, startDestination = "root") {
        appGraph(
            navController = controller,
            startDestination,
            stateOwner = stateOwner
        )
    }
}
