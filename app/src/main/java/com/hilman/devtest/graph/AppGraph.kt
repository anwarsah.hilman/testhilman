package com.hilman.devtest.graph


import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import com.hilman.domain.usecase.employee.model.EmployeeModel
import com.hilman.ui.screen.AddEmployeeScreen
import com.hilman.ui.screen.AddEmployeeViewModel
import com.hilman.ui.screen.ListEmployeeScreen
import com.hilman.ui.screen.ListEmployeeViewModel
import com.hilman.ui.screen.UpdateEmployeeScreen
import com.hilman.ui.screen.UpdateEmployeeViewModel

@ExperimentalComposeUiApi
@ExperimentalLayoutApi
@ExperimentalMaterialApi
@ExperimentalMaterial3Api
fun NavGraphBuilder.appGraph(
    navController: NavController,
    startDestination: String,
    routeName: String = "root",
    stateOwner: ViewModelStoreOwner
) {

    navigation(startDestination = startDestination, route = routeName) {
        composable(route = "list") {

            val vm: ListEmployeeViewModel = hiltViewModel(stateOwner)
            ListEmployeeScreen(navController = navController, vm = vm)
        }

        composable(route = "add") {
            val vm: AddEmployeeViewModel = hiltViewModel()
            AddEmployeeScreen(navController, vm)
        }

        composable(route = "edit") {
            val vm: UpdateEmployeeViewModel = hiltViewModel()
            val listVm: ListEmployeeViewModel = hiltViewModel(stateOwner)
            val args =
                navController?.currentBackStackEntry?.arguments?.getParcelable<EmployeeModel>("data")
            vm.age.value = args?.employeeAge.orEmpty()
            vm.name.value = args?.employeeName.orEmpty()
            vm.salary.value = args?.employeeSalary.orEmpty()
            vm.id.value = args?.id?.toString() ?: ""
            UpdateEmployeeScreen(navController, vm,listVm,args)
        }

    }


}
