package com.hilman.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.hilman.data.remote.EmployeeDataSource
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class EmployeeApiTest {


    @Inject
    lateinit var employeeDataSource: EmployeeDataSource

    private val delay: Long = 60

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun start() {
        hiltRule.inject()
    }


    @Test
    fun testGetEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val result = employeeDataSource.getEmployee()
            assert(result.isSuccess)
        }
    }

    @Test
    fun testGetEmployeeByUId() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val result = employeeDataSource.getEmployee("1")
            assert(result.isSuccess) {
                result.exceptionOrNull().toString()
            }
        }
    }

    @Test
    fun testCreateEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val result = employeeDataSource.createEmployee("1", "Hilz", "20000")
            assert(result.isSuccess) {
                result.exceptionOrNull().toString()
            }
        }
    }

    @Test
    fun testEditEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val result = employeeDataSource.updateEmployee("1", "30", "Hilzz", "200000")
            assert(result.isSuccess) {
                result.exceptionOrNull().toString()
            }
        }
    }

    @Test
    fun testCreateAndDeleteEmployee() {
        runBlocking {
            delay(TimeUnit.SECONDS.toMillis(delay))
            val result = employeeDataSource.createEmployee("1", "Hilz", "20000")
            assert(result.isSuccess) {
                result.exceptionOrNull().toString()
            }
            delay(TimeUnit.SECONDS.toMillis(delay))
            val deleteResult =
                employeeDataSource.deleteEmployee(result.getOrThrow().id?.toString().orEmpty())
            assert(deleteResult.isSuccess) {
                deleteResult.exceptionOrNull().toString()
            }
        }
    }


}