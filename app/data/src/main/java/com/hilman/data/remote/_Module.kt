package com.hilman.data.remote

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
class _Module {

    @Provides
    fun provideEmployeeApi(retrofit: Retrofit) = retrofit.create(EmployeeApi::class.java)

    @Provides
    fun provideEmployeeDataSource(employeeApi: EmployeeApi): EmployeeDataSource =
        EmployeeDataSourceRetrofit(employeeApi)


}