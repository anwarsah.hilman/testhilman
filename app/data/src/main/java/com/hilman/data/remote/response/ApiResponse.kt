package com.hilman.data.remote.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiResponse<out T>(
    @field:Json(name = "status")
    val status: String? = null,

    @field:Json(name = "message")
    val message: String? = null,

    @field:Json(name = "data")
    val data: T? = null
)