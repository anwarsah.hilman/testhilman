package com.hilman.data.remote

import com.hilman.data.remote.response.employee.Employee

interface EmployeeDataSource {

    suspend fun getEmployee(): Result<List<Employee>>
    suspend fun getEmployee(id: String): Result<Employee>
    suspend fun createEmployee(age: String, name: String, salary: String): Result<Employee>

    suspend fun updateEmployee(
        id: String,
        age: String,
        name: String,
        salary: String
    ): Result<Employee>

    suspend fun deleteEmployee(id: String): Result<Any>
}