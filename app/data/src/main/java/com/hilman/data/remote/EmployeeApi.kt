package com.hilman.data.remote

import com.hilman.data.remote.response.ApiResponse
import com.hilman.data.remote.response.employee.Employee
import com.hilman.data.remote.response.employee.EmployeeRequest
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface EmployeeApi {

    @GET("employees")
    suspend fun getEmployee(): Response<ApiResponse<List<Employee>>>

    @GET("employee/{id}")
    suspend fun getEmployee(@Path("id") id: String): Response<ApiResponse<Employee>>

    @POST("create")
    suspend fun createEmployee(@Body request: EmployeeRequest): Response<ApiResponse<Employee>>


    @PUT("update/{id}")
    suspend fun updateEmployee(
        @Path("id") id: String,
        @Body request: EmployeeRequest
    ): Response<ApiResponse<Employee>>

    @DELETE("delete/{id}")
    suspend fun deleteEmployee(
        @Path("id") id: String
    ): Response<ApiResponse<Any>>


}