package com.hilman.data.remote

import com.hilman.data.ext.asResult
import com.hilman.data.remote.response.employee.Employee
import com.hilman.data.remote.response.employee.EmployeeRequest


class EmployeeDataSourceRetrofit(private val employeeApi: EmployeeApi) : EmployeeDataSource {

    override suspend fun getEmployee(): Result<List<Employee>> =
        employeeApi.getEmployee().asResult()

    override suspend fun getEmployee(id: String): Result<Employee> =
        employeeApi.getEmployee(id).asResult()

    override suspend fun createEmployee(
        age: String, name: String, salary: String
    ): Result<Employee> = employeeApi.createEmployee(
        EmployeeRequest(
            age, name, salary
        )
    ).asResult()

    override suspend fun updateEmployee(
        id: String, age: String, name: String, salary: String
    ): Result<Employee> =
        employeeApi.updateEmployee(id, EmployeeRequest(age, name, salary)).asResult()

    override suspend fun deleteEmployee(id: String): Result<Any> =
        employeeApi.deleteEmployee(id).asResult()


}