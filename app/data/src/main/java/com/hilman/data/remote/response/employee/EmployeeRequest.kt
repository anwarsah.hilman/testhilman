package com.hilman.data.remote.response.employee


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EmployeeRequest(
    @Json(name = "age")
    val age: String?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "salary")
    val salary: String?
)