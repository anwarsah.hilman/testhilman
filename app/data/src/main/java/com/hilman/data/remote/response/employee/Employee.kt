package com.hilman.data.remote.response.employee


import com.squareup.moshi.Json

data class Employee(
    @Json(name = "employee_age")
    val employeeAge: String?,
    @Json(name = "employee_name")
    val employeeName: String?,
    @Json(name = "employee_salary")
    val employeeSalary: String?,
    @Json(name = "id")
    val id: Int?,
    @Json(name = "profile_image")
    val profileImage: String?
)