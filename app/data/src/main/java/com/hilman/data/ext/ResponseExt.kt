package com.hilman.data.ext

import com.hilman.data.remote.response.ApiResponse
import retrofit2.Response

fun <T> Response<ApiResponse<T>>.asResult(): Result<T> {
    return if (isSuccessful) {
        this.body()?.let {
            if (it.status == "success") {
                it.data?.let { data ->
                    Result.success(data)
                } ?: run {
                    Result.failure(Exception("Empty Response"))
                }
            } else {
                Result.failure(Exception("Error"))
            }
        } ?: Result.failure(Exception("Empty Response"))
    } else {
        if (this.code() == 429) {
            Result.failure(Exception("Too many Request"))
        } else {

            Result.failure(Exception(this.errorBody()?.string().orEmpty()))
        }
    }
}

