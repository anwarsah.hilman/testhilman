package com.hilman.data.remote

import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.time.Duration
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
class _ApiModule {


    @Provides
    @Named("baseUrl")
    fun baseUrl(): String = "https://dummy.restapiexample.com/api/v1/"


    @Provides
    fun timeout() = Duration.ofMinutes(1)

    @Provides
    fun provideOkhttp(
        @ApplicationContext context: Context,

        timeout: Duration
    ): OkHttpClient = OkHttpClient.Builder().apply {
        connectTimeout(timeout)
        readTimeout(timeout)
        addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        writeTimeout(timeout)
        callTimeout(timeout)


    }.build()

    @Provides
    fun provideRetrofit(
        client: OkHttpClient,
        @Named("baseUrl") baseUrl: String,
        moshi: Moshi
    ): Retrofit = createRetrofit(client, baseUrl, moshi)

    @Provides
    fun createMoshi() = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    fun createRetrofit(client: OkHttpClient, baseUrl: String, moshi: Moshi): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(
                MoshiConverterFactory.create(moshi)
                    .asLenient()
                    .withNullSerialization()
            )
            .client(client)
            .build()
}